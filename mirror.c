#include <stdio.h>
#include <stdlib.h>

struct node {
    struct node *left;
    struct node *right;
    int val;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printRecur(struct node *root, int *path, int len)
{
    if (root == NULL)
        return;

    path[len] = root->val;
    len++;

    if (root->left == NULL && root->right == NULL) {
        int i = 0;
        while (i < len) {
            printf("%d-->", path[i]);
            i++;
        }
        printf("\n");
    }

    printRecur(root->left, path, len);
    printRecur(root->right, path, len);
}

void printpaths(struct node *root)
{
    int path[4096] = {0};
    printRecur(root, path, 0);
}

void mirror(struct node *root)
{
    if (root == NULL)
        return;

    struct node *temp = root->right;
    root->right = root->left;
    root->left = temp;

    mirror(root->left);
    mirror(root->right);
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 10);
    root = insert(root, 3);
    root = insert(root, 1);
    root = insert(root, 8);
    root = insert(root, 6);
    root = insert(root, 2);
    root = insert(root, 13);

    printpaths(root);

    mirror(root);

    printpaths(root);

    return 0;
}
