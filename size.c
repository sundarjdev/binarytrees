#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf("<--%d-->", root->val);
    printtree(root->right);
}

int size(struct node *root)
{
    if (root == NULL)
        return 0;

    return (size(root->left) + 1 + size(root->right));
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 4);
    root = insert(root, 2);
    root = insert(root, 1);
    root = insert(root, 3);
    root = insert(root, 6);
    root = insert(root, 5);
    root = insert(root, 7);

    printtree(root);
    printf("\n");

    printf("Binary Tree size = %d\n", size(root));

    return 0;
}
