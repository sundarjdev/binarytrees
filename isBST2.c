#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

int minValue(struct node *root)
{
    if (root->left == NULL)
        return root->val;
    else
        return minValue(root->left);
}

int maxValue(struct node *root)
{
    if (root->right == NULL)
        return root->val;
    else
        return maxValue(root->right);
}

enum {
    true,
    false,
};

int isBSTRecur(struct node *root, int min, int max)
{
    if (root == NULL)
        return true;

    if (root->val >= max || root->val < min)
        return false;

    return (isBSTRecur(root->left, min, root->val) || isBSTRecur(root->right, root->val, max));
}

int isBST(struct node *root)
{
    return isBSTRecur(root, INT_MIN, INT_MAX);
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 5);
    root = insert(root, 2);
    root = insert(root, 7);
    root = insert(root, 1);
    root->left->right = newNode(6);

    printf("isBST: %d\n", isBST(root));

    return 0;
}
