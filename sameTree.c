#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *right;
    struct node *left;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printRecur(struct node *root, int *path, int len)
{
    if (root == NULL)
        return;

    path[len] = root->val;
    len++;

    if (root->left == NULL && root->right == NULL) {
        int i = 0;
        while (i < len) {
            printf("%d-->", path[i]);
            i++;
        }
        printf("\n");
        return;
    }

    printRecur(root->left, path, len);
    printRecur(root->right, path, len);
}

void printpaths(struct node *root)
{
    int path[4096];
    printRecur(root, path, 0);
}

enum {
    true,
    false,
};

int doubleTree(struct node *a, struct node *b)
{
    if (a == NULL && b == NULL)
        return true;
    else {
        if (a == NULL || b == NULL)
            return false;

        if (a->val != b->val)
            return false;

        return (doubleTree(a->left, b->left) || doubleTree(a->right, b->right));
    }
}

int main(void)
{
    struct node *root1 = NULL;
    struct node *root2 = NULL;
    struct node *root3 = NULL;

    root1 = insert(root1, 10);
    root1 = insert(root1, 1);
    root1 = insert(root1, 3);
    root1 = insert(root1, 8);
    root1 = insert(root1, 2);
    root1 = insert(root1, 11);
    root1 = insert(root1, 13);

    root2 = insert(root2, 10);
    root2 = insert(root2, 1);
    root2 = insert(root2, 3);
    root2 = insert(root2, 8);
    root2 = insert(root2, 2);
    root2 = insert(root2, 11);
    root2 = insert(root2, 13);

    root3 = insert(root3, 10);
    root3 = insert(root3, 1);
    root3 = insert(root3, 3);
    root3 = insert(root3, 8);
    root3 = insert(root3, 2);
    root3 = insert(root3, 11);

    printpaths(root1);
    printpaths(root2);
    printpaths(root3);

    printf("Double Tree: %d\n", doubleTree(root1, root2));
    printf("Double Tree: %d\n", doubleTree(root1, NULL));
    printf("Double Tree: %d\n", doubleTree(root1, root3));

    return 0;
}
