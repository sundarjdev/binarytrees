#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);
    else if (val <= root->val)
        root->left = insert(root->left, val);
    else if (val > root->val)
        root->right = insert(root->right, val);

    return root;
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf("<--%d-->", root->val);
    printtree(root->right);
}

void printrecur(struct node *root, int path[], int pathlen);
void printPaths(struct node *root)
{
    int path[4096] = {0};
    printrecur(root, path, 0);
}

void printrecur(struct node *root, int *path, int pathlen)
{
    path[pathlen] = root->val;
    pathlen++;

    if (root->left == NULL && root->right == NULL) {
        for (int i = 0; i < pathlen; i++)
            printf("%d-->", path[i]);
        printf("\n");
        return;
    }

    if (root->left != NULL)
        printrecur(root->left, path, pathlen);

    if (root->right != NULL)
        printrecur(root->right, path, pathlen);
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 10);
    root = insert(root, 2);
    root = insert(root, 8);
    root = insert(root, 6);
    root = insert(root, 7);
    root = insert(root, 9);
    root = insert(root, 11);

    printtree(root);
    printf("\n");

    printPaths(root); 

    return 0;
}
