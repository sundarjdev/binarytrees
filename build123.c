#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;

    return node;
}

struct node *build123a()
{
    struct node *root = NULL;

    root = newNode(2);
    root->left = newNode(1);
    root->right = newNode(3);

    return root;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

struct node *build123b()
{
    struct node *root = NULL;

    root = insert(root, 2);
    root = insert(root, 1);
    root = insert(root, 3);
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf("<--%d--> ", root->val);
    printtree(root->right);
}

int main(void)
{
    struct node *root = NULL;

    root = build123b();

    printtree(root);
    printf("\n");

    return 0;
}
