#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printRecur(struct node *root, int *path, int len)
{
    if (root == NULL)
        return;

    path[len] = root->val;
    len++;

    if (root->left == NULL && root->right == NULL) {
        int i = 0;
        while (i < len) {
            printf("%d-->", path[i]);
            i++;
        }
        printf("\n");
    }

    printRecur(root->left, path, len);
    printRecur(root->right, path, len);
}

void printpaths(struct node *root)
{
    int path[4096];
    printRecur(root, path, 0);
}

void doubleTree(struct node *root)
{
    if (root == NULL)
        return;

    struct node *temp = root->left;
    root->left = newNode(root->val);
    root->left->left = temp;

    doubleTree(temp);
    doubleTree(root->right);
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 10);
    root = insert(root, 3);
    root = insert(root, 1);
    root = insert(root, 5);
    root = insert(root, 4);
    root = insert(root, 9);
    root = insert(root, 13);

    printpaths(root);

    doubleTree(root);

    printpaths(root);

    return 0;
}
