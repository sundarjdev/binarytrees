#include <stdio.h>
#include <stdlib.h>

enum { true, false };

struct node {
    struct node *left;
    struct node *right;
    int val;
};

struct node *newNode(int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val) {
        root->left = insert(root->left, val);
    } else {
        root->right = insert(root->right, val);
    }
    return root;
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf("<--%d-->", root->val);
    printtree(root->right);
}

int hasPathSum(struct node *root, int sum)
{
    if (root == NULL)
        return (sum == 0);

    sum = sum - root->val;
    return (hasPathSum(root->left, sum) || hasPathSum(root->right, sum));
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 5);
    root = insert(root, 4);
    root = insert(root, 8);
    root = insert(root, 1);
    root = insert(root, 14);
    root = insert(root, 2);

    printtree(root);
    printf("\n");

    printf("hasPathSum(12): %d\n", hasPathSum(root, 12));
    printf("hasPathSum(10): %d\n", hasPathSum(root, 10));
    printf("hasPathSum(27): %d\n", hasPathSum(root, 27));
    printf("hasPathSum(5): %d\n", hasPathSum(root, 5));

    return 0;
}
