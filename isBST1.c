#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

int minValue(struct node *root)
{
    if (root->left == NULL)
        return root->val;
    else
        return minValue(root->left);
}

int maxValue(struct node *root)
{
    if (root->right == NULL)
        return root->val;
    else
        return maxValue(root->right);
}

enum {
    true,
    false,
};

int isBST(struct node *root)
{
    if (root == NULL)
        return true;

    if (root->left) {
        int l_max = maxValue(root->left); // find max of left subtree
        if (l_max > root->val)
            return false;
    }

    if (root->right) {
        int r_min = minValue(root->right); // find min of right subtree
        if (r_min <= root->val)
            return false;
    }

    return (isBST(root->left) || isBST(root->right));
}

int main(void)
{
    struct node *root = NULL;

    root = insert(root, 5);
    root = insert(root, 2);
    root = insert(root, 7);
    root = insert(root, 1);
    //root->left->right = newNode(6);

    printf("isBST: %d\n", isBST(root));

    return 0;
}
