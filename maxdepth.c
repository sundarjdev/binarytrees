#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);

    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf(" %d ", root->val);
    printtree(root->right);
}

int maxDepth(struct node *root)
{
    if (root == NULL)
        return 0;

    int ldepth = maxDepth(root->left);
    int rdepth = maxDepth(root->right);

    if (ldepth == rdepth)
        return (ldepth + 1); // ltree depth + 1 for root
    else if (ldepth > rdepth)
        return (ldepth + 1);
    else
        return (rdepth + 1);
}

int main (void)
{
    struct node *root = NULL;

    root = insert(root, 1);
    root = insert(root, 2);
    root = insert(root, 3);
    root = insert(root, 4);
    root = insert(root, 5);
    root = insert(root, 6);
    root = insert(root, 7);

    printtree(root);
    printf("\n");

    printf("maxdepth is: %d\n", maxDepth(root));

    return 0;
}
