#include <stdio.h>

int countTree(int key)
{
    if (key <= 1)
        return 1;
    else {
        int sum = 0;
        for (int i = 1; i <= key; i++) {
            sum += (countTree(i - 1) * countTree(key - i));
        }
        return sum;
    }
}

int main(void)
{
    printf("countTree(1): %d\n", countTree(1));
    printf("countTree(2): %d\n", countTree(2));
    printf("countTree(3): %d\n", countTree(3));
    printf("countTree(4): %d\n", countTree(4));
    printf("countTree(5): %d\n", countTree(5));

    return 0;
}
