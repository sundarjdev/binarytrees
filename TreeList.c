#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *left;
    struct node *right;
};

struct node *newNode(int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->left = node->right = NULL;
    return node;
}

struct node *insert(struct node *root, int val)
{
    if (root == NULL)
        return newNode(val);
    
    if (val <= root->val)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);

    return root;
}

void printtree(struct node *root)
{
    if (root == NULL)
        return;

    printtree(root->left);
    printf("<--%d-->", root->val);
    printtree(root->right);
}

void printlist(struct node *head)
{
    if (head == NULL)
        return;

    struct node *tail = head->left;

    while (head != tail) {
        printf("%d-->", head->val);
        head = head->right;
    }

    printf("%d-->\n", head->val);
}

struct node *append(struct node *head1, struct node *head2)
{
    if (head1 == NULL) {
        return head2;
    }

    if (head2 == NULL) {
        return head1;
    }

    struct node *temp2 = head2->left;
    head1->left->right = head2;
    head2->left = head1->left;

    head1->left = temp2;
    temp2->right = head1;

    return head1;
}

struct node *tree_to_list(struct node *root)
{
    if (root == NULL)
        return NULL;

    struct node *temp_r = root->right;
    struct node *temp_l = root->left;

    root->left = root->right = root;

    struct node *llist = tree_to_list(temp_l);
    llist = append(llist, root);

    struct node *rlist = tree_to_list(temp_r);
    return append(llist, rlist);
}

int main(void)
{
    struct node *root = NULL;
    struct node *head = NULL;

    root = insert(root, 4);
    root = insert(root, 5);
    root = insert(root, 2);
    //root = insert(root, 1);
    root = insert(root, 3);

    printtree(root);
    printf("\n");

    head = tree_to_list(root);

    printlist(head);

    return 0;
}
